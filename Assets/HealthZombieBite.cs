using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyPooler;
public class HealthZombieBite : MonoBehaviour
{
    public float health = 5;


    private bool canDie = true;
    Animator anim;

    AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (health <= 0f && canDie)
        {
            Dead();
            canDie = false;
        }
    }
    public void TakeDamage(float damage)
    {
        health -= damage;
    }

    public void Dead()
    {
        ObjectPooler.Instance.GetFromPool("Blood_Explosion", transform.position, transform.rotation);
        anim.SetTrigger("dead");
        Destroy(gameObject, 5f);
        audioSource.Stop();
    }
}



