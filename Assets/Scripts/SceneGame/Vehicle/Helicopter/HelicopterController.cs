using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HelicopterController : MonoBehaviour
{
    private float speed = 20f;
    private float timeAlive = 35f;
    private bool jump;
    float timeAllowJump = 0;
    float timeRate = 20f;
    AudioSource sound;
    [SerializeField] private GameObject cameraHelicopter;
    [SerializeField] private GameObject SpawnPosition;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject mainCamera;
    [SerializeField] private GameObject zombieSpawnPoint;
    [SerializeField] private GameObject textGuideGame;
    private void Awake()
    {
       Init();
    }
    private void Start()
    {
        zombieSpawnPoint.SetActive(false);
    }
    private void Init()
    {
        player.SetActive(false);
        mainCamera.SetActive(false);
        sound = GetComponent<AudioSource>();
        jump = true;
        timeAllowJump = Time.time + timeRate;
    }
    // Update is called once per frame
    void Update()
    {
        Fly();
        ShowTextGuide();
        if (Input.GetKeyDown(KeyCode.Space) && (Time.time > timeAllowJump ))
        {
            JumpOut();
            Destroy(gameObject, timeAlive);
            sound.Stop();
        }
    }

    void JumpOut()
    {
        if (jump == true)
        {
            player.transform.position = SpawnPosition.transform.position;
            player.transform.rotation = SpawnPosition.transform.rotation;
            player.SetActive(true);
            mainCamera.SetActive(true);
            DestroyImmediate(cameraHelicopter);
            DestroyImmediate(textGuideGame);
            StartSpwanZombie();
        }

        jump = false;
    }


    void ShowTextGuide()
    {
        if (Time.time > timeAllowJump)
        {
            if (jump == true)
            {
                textGuideGame.SetActive(true);
            }
        }
    }

    private void Fly()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    private void StartSpwanZombie()
    {
       this.zombieSpawnPoint.gameObject.SetActive(true);
    }

}
