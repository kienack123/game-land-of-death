using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnZombie : MonoBehaviour
{
    public float timeSpawn;
    public GameObject[] zombieGirl;
    [SerializeField] private GameObject[] spawnPoint;

    [SerializeField] private float minSpawnTime = 0.2f;
    [SerializeField] private float maxSpawntime = 2;
    
    private float lastSpawntime = 0;
    private float spawnTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        spawnPoint = GameObject.FindGameObjectsWithTag("SpawnPoint");
        UpdateSpawnTime();
    }


    
    // Update is called once per frame
    void Update()
    {
        StartSpawn();

    }
    void UpdateSpawnTime()
    {
        lastSpawntime = Time.time;
        spawnTime = Random.Range(minSpawnTime, maxSpawntime);
    }

    void Spawn()
    {
        int point = Random.Range(0, spawnPoint.Length);
        int zombieIndex = Random.Range(0, zombieGirl.Length);
        var Spawn = Instantiate(zombieGirl[zombieIndex], spawnPoint[point].transform.position, Quaternion.identity);
        UpdateSpawnTime();
        
    }


    void StartSpawn()
    {
        StartCoroutine(SpawnDelay());
    }

    IEnumerator SpawnDelay()
    {
        yield return new WaitForSeconds(timeSpawn);
        if (Time.time >= lastSpawntime + spawnTime)
        {
            Spawn();
        }
        yield return new WaitForSeconds(20f);
        gameObject.SetActive(false);
    }
}
