using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleStateZombie : StateMachineBehaviour
{
    Transform player;
    private float timer;
    public float chaseRange = 8;
    public float randomIndex;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 0;
        animator.SetFloat("randomAnimIdle", Random.Range(0, randomIndex));
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

 
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer += Time.deltaTime;
        if(timer > 5)
        {
            animator.SetBool("isPatrolling", true);
        }


        float distance = Vector3.Distance(player.position, animator.transform.position);
        if(distance < chaseRange)
        {
            animator.SetBool("isChasing", true);
        }
    }


    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
