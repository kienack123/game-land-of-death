using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using MyPooler;

public class HealthZombie : MonoBehaviour
{
    public GameObject head; 

    public float health = 5f;

    public TextKilledPlayer textKilledPlayer;

    NavMeshAgent agent;

    private bool canDie ;

    private bool headShot = false ; 

    CapsuleCollider coll;

    private const string blood_Explosion = "Blood_Explosion";

    Animator anim;

    AudioSource audioSource;

    private void OnEnable()
    {
        canDie = true;
    }

    void Start()
    {
        audioSource = GetComponent<AudioSource>();  

        anim = GetComponent<Animator>();
      
        agent = GetComponent<NavMeshAgent>();

        coll = GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckDead();
    }

    private void CheckDead()
    {
        if (health <= 0f && canDie)
        {
            Dead();
        }
        if (health <= 0f && canDie && headShot)
        {
            HeadShotDead();
        }
    }



    public void TakeDamage(float damage) 
    {
        health -= damage;
    }

    public void Dead()
    {
        anim.SetTrigger("dead");
        agent.isStopped = true;
        canDie = !canDie;
        coll.enabled = false;
        textKilledPlayer.UpdateKill();
        audioSource.Stop();
        Destroy(gameObject, 10f);
        
    }

    public void HeadShotDead()
    {
        ObjectPooler.Instance.GetFromPool(blood_Explosion, head.transform.position, Quaternion.identity);
        head.gameObject.SetActive(false);
        headShot = true;
        Dead();
    }
}
