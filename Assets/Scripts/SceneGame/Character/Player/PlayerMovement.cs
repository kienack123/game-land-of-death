using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
public class PlayerMovement : vThirdPersonMotor
{
    private vMovementSpeed speed;
    private void Awake()
    {
        speed = GetComponentInChildren<vMovementSpeed>();
    }
    public void Walking()
    {
        speed.walkByDefault = true;
    }
    public void NotWalking()
    {
        speed.walkByDefault = false;
    }
}
