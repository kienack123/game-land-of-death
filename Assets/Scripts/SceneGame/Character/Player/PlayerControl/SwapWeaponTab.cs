using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapWeaponTab : MonoBehaviour
{
    public TimeManager timeManager;

    [SerializeField] private HideCursor cursor;

    [SerializeField] private GameObject mainCamera;
    vThirdPersonCamera cam;

    [SerializeField] private GameObject WeaponWheelTab;
    // Start is called before the first frame update

    private void Awake()
    {
        if (mainCamera != null)
        {
            cam = mainCamera.GetComponent<vThirdPersonCamera>();
        }
      
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            SoundController.instance.PlayThisSound("ChangeWeapon",1f);
            WeaponWheelTab.SetActive(true);
            timeManager.StartSlowTime();
            cursor.Show();
            LockCamera();
        }
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            WeaponWheelTab.SetActive(false);
            timeManager.ReturnRealTime();
            cursor.Hide();
            UnLockCamera();
        }
    }


    void LockCamera()
    {
        cam.xMouseSensitivity = 0.1f;
        cam.yMouseSensitivity = 0.1f;
    }

    void UnLockCamera()
    {
        cam.xMouseSensitivity = 3f;
        cam.yMouseSensitivity = 3f;
    }


 


}
