using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum WeaponType
{
    GunMachine,
    Pistol,
    Katana,
    Grenade
}
public class SwapWeapon : MonoBehaviour
{
    public WeaponType weaponType;

    [SerializeField] private GameObject Katana;
    [SerializeField] private GameObject KatanaMesh;
    [SerializeField] private GameObject Gun;
    [SerializeField] private GameObject GunMachine;
    [SerializeField] private GameObject GunMachineHandle;
    [SerializeField] private GameObject Grenade;
  


    [SerializeField] private GameObject Hidden1;
    [SerializeField] private GameObject Hidden2;
    [SerializeField] private GameObject Hidden3;
    [SerializeField] private GameObject Hidden4;

    [SerializeField] private GameObject AmmoGunMachineUI;
    [SerializeField] private GameObject AmmoGunUI;
    [SerializeField] private GameObject AmmoGrenadeUI;

    Animator anim;
    public bool isDrawing = false;
    // Start is called before the first frame update
    private void Awake()
    {
        this.gameObject.GetComponent<PlayerGunMachine>().enabled = false;
        this.gameObject.GetComponent<PlayerPistol>().enabled = false;
        this.gameObject.GetComponent<PlayerKatana>().enabled = false;
        this.gameObject.GetComponent<PlayerGrenade>().enabled = false;
        anim = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    protected void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (!isDrawing)
            {
                SoundController.instance.PlayThisSound("ChangeWeapon", 1f);
                SwapGunMachine();
            } 
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (!isDrawing)
            {
                SoundController.instance.PlayThisSound("ChangeWeapon", 1f);
                SwapGun();
            }
           
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (!isDrawing)
            {
                SoundController.instance.PlayThisSound("ChangeWeapon", 1f);
                anim.SetTrigger("drawingKatana");
            }
            
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
                SoundController.instance.PlayThisSound("ChangeWeapon", 1f);
                SwapGrenade();    
        }
    }

    
    protected void DrawingWeapon()
    {
        isDrawing = true;
    }

    protected void DrawedWeapon()
    {
        isDrawing = false;
    }

    public void SwapGunMachine()
    {
        this.Katana.gameObject.SetActive(false);
        this.KatanaMesh.gameObject.SetActive(true);
        this.Gun.gameObject.SetActive(false);
        this.GunMachineHandle.gameObject.SetActive(false);
        this.GunMachine.gameObject.SetActive(true);
        this.Grenade.gameObject.SetActive(false);
        this.gameObject.GetComponent<PlayerGunMachine>().enabled = true;
        this.gameObject.GetComponent<PlayerPistol>().enabled = false;
        this.gameObject.GetComponent<PlayerKatana>().enabled = false;
        this.gameObject.GetComponent<PlayerGrenade>().enabled = false;
        ShowAmmoGunMachineUI();
        ShowWeapon1();
    }


    public void SwapGun()
    {       
        
        this.Katana.gameObject.SetActive(false);
        this.KatanaMesh.gameObject.SetActive(true);
        this.Gun.gameObject.SetActive(true);
        this.GunMachineHandle.gameObject.SetActive(true);
        this.GunMachine.gameObject.SetActive(false);
        this.gameObject.GetComponent<PlayerGunMachine>().enabled = false;
        this.gameObject.GetComponent<PlayerPistol>().enabled = true;
        this.gameObject.GetComponent<PlayerKatana>().enabled = false;
        this.gameObject.GetComponent<PlayerGrenade>().enabled = false;
        ShowAmmoGunUI();
        ShowWeapon2();
    }


    public void SwapKatana()
    {
        SoundController.instance.PlayThisSound("Character/DrawnKatana", 1f);
        this.Katana.gameObject.SetActive(true);
        this.KatanaMesh.gameObject.SetActive(false);
        this.Gun.gameObject.SetActive(false);
        this.GunMachineHandle.gameObject.SetActive(true);
        this.GunMachine.gameObject.SetActive(false);
        this.Grenade.gameObject.SetActive(false);
        this.gameObject.GetComponent<PlayerGunMachine>().enabled = false;
        this.gameObject.GetComponent<PlayerPistol>().enabled = false;
        this.gameObject.GetComponent<PlayerKatana>().enabled = true;
        this.gameObject.GetComponent<PlayerGrenade>().enabled = false;
        ShowWeapon3();
    }

    public void SwapGrenade()
    {
        this.Katana.gameObject.SetActive(false);
        this.KatanaMesh.gameObject.SetActive(true);
        this.Gun.gameObject.SetActive(false);
        this.GunMachineHandle.gameObject.SetActive(true);
        this.GunMachine.gameObject.SetActive(false);
        this.Grenade.gameObject.SetActive(true);
        this.gameObject.GetComponent<PlayerGunMachine>().enabled = false;
        this.gameObject.GetComponent<PlayerPistol>().enabled = false;
        this.gameObject.GetComponent<PlayerKatana>().enabled = false;
        this.gameObject.GetComponent<PlayerGrenade>().enabled = true;
        ShowAmmoGrenadeUI();
        ShowWeapon4();
        
    }

    private void ShowWeapon1()
    {
        Hidden1.SetActive(true);
        Hidden2.SetActive(false);
        Hidden3.SetActive(false);
        Hidden4.SetActive(false);
    }


    private void ShowWeapon2()
    {
        Hidden1.SetActive(false);
        Hidden2.SetActive(true);
        Hidden3.SetActive(false);
        Hidden4.SetActive(false);
    }
    private void ShowWeapon3()
    {
        Hidden1.SetActive(false);
        Hidden2.SetActive(false);
        Hidden3.SetActive(true);
        Hidden4.SetActive(false);
    }
    private void ShowWeapon4()
    {
        Hidden1.SetActive(false);
        Hidden2.SetActive(false);
        Hidden3.SetActive(false);
        Hidden4.SetActive(true);
    }

    private void ShowAmmoGunMachineUI()
    {
        this.AmmoGunMachineUI.gameObject.SetActive(true);
        this.AmmoGunUI.gameObject.SetActive(false);
        this.AmmoGrenadeUI.gameObject.SetActive(false);

    }

    private void ShowAmmoGunUI()
    {
        this.AmmoGunUI.gameObject.SetActive(true);
        this.AmmoGunMachineUI.gameObject.SetActive(false);
        this.AmmoGrenadeUI.gameObject.SetActive(false);
    }

    private void ShowAmmoGrenadeUI()
    {
        this.AmmoGrenadeUI.gameObject.SetActive(true);
        this.AmmoGunUI.gameObject.SetActive(false);
        this.AmmoGunMachineUI.gameObject.SetActive(false);
    }
}
