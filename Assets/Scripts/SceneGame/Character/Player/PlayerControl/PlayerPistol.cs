using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using MyPooler;
using UnityEngine.UI;
public class PlayerPistol : PlayerShooter
{
    [SerializeField] Pistol pistol;
    SwapWeapon swapWeapon;
    private void Awake()
    {
        swapWeapon = GetComponent<SwapWeapon>();

        anim = GetComponent<Animator>();

        playerMovement = GetComponent<vThirdPersonController>();
    }
    private void FixedUpdate()
    {
       
        if (Input.GetMouseButton(1))
        {
            if (!swapWeapon.isDrawing)
            {
                this.Aim();
            }
        }
        else
        {
            this.NotAim();
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (!swapWeapon.isDrawing)
            {
                this.Reload();
            }    
        }
    }
    protected override void Aim()
    {
        base.Aim();
        anim.SetBool(animatorName.AIM_PISTOL, true);

        // Shoot
        this.ShootAiming();
    }

    protected override void NotAim()
    {
        base.NotAim();
        anim.SetBool(animatorName.AIM_PISTOL, false);
    }



    protected override void ShootAiming()
    {
        Vector2 screenCenter = new Vector2(Screen.width / 2, Screen.height / 2);

        Ray ray = Camera.main.ScreenPointToRay(screenCenter);
        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, aimLayerMask))
        {
            aimPosition.transform.position = Vector3.Lerp(aimPosition.transform.position, hit.point, aimSmoothSpeed * Time.deltaTime);
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (pistol.currentAmmo > 0 )
            {
                pistol.Shoot(nameSound.SOUND_SHOOT, nameSound.SOUND_DROP_SHELL);
                if ((hit.collider.CompareTag(TAG_NAME_ZOMBIE)) && (!hit.collider.CompareTag(TAG_NAME_GORES_OBJ)))
                {
                    GameObject gores = ObjectPooler.Instance.GetFromPool(TAG_NAME_GORES_OBJ, hit.point + hit.normal * 0.001f, Quaternion.identity) as GameObject;
                    gores.transform.LookAt(hit.point + hit.normal);
                }
            }
            else if (pistol.currentAmmo <= 0) 
            {
                pistol.DryOutAmmo();
            }
        }
    }

    protected override void Reload()
    {
        if (!isReloading)
        {
            if (pistol.currentAmmo < pistol.maxAmmo)
            {
                base.StartReload();
                anim.SetTrigger(animatorName.RELOAD_PISTOL);
                pistol.Reload();
            }
        }
    }
    
}
