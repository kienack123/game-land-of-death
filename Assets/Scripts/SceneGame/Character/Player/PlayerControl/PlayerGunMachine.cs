using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using MyPooler;
public class PlayerGunMachine : PlayerShooter
{
    [SerializeField] GunMachine gunMachine;

    SwapWeapon swapWeapon;
    private void Awake()
    {
        anim = GetComponent<Animator>();

        playerMovement = GetComponent<vThirdPersonController>();

        swapWeapon = GetComponent<SwapWeapon>();
    }
    private void FixedUpdate()
    {
        
        if (Input.GetMouseButton(1))
        {
            if (!swapWeapon.isDrawing)
            {
                this.Aim();
            }     
        }
        else
        {
            this.NotAim();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (!swapWeapon.isDrawing)
            {
                this.Reload();
            }
            
        }
    }
    protected override void Aim()
    {
        base.Aim();
        anim.SetBool(animatorName.AIM_MACHINE_GUN, true);
        this.ShootAiming();
    }

    protected override void NotAim()
    {
        base .NotAim();
        anim.SetBool(animatorName.AIM_MACHINE_GUN, false);
    }


    protected override void ShootAiming()
    {
        Vector2 screenCenter = new Vector2(Screen.width / 2, Screen.height / 2);

        Ray ray = Camera.main.ScreenPointToRay(screenCenter);
        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, aimLayerMask))
        {
            aimPosition.transform.position = Vector3.Lerp(aimPosition.transform.position, hit.point, aimSmoothSpeed * Time.deltaTime);
        }
        if (Input.GetMouseButton(0))
        {     
            if (gunMachine.currentAmmo > 0 )
            {
                gunMachine.Shoot(nameSound.SOUND_SHOOT,nameSound.SOUND_DROP_SHELL);
                if ((hit.collider.CompareTag(TAG_NAME_ZOMBIE)) && (!hit.collider.CompareTag(TAG_NAME_GORES_OBJ)))
                {
                    GameObject gores = ObjectPooler.Instance.GetFromPool(TAG_NAME_GORES_OBJ, hit.point + hit.normal * 0.001f, Quaternion.identity) as GameObject;
                    gores.transform.LookAt(hit.point + hit.normal);
                }
            }
            else if (gunMachine.currentAmmo <= 0 )
            {
                gunMachine.DryOutAmmo();
            }
        }
    }
    protected override void Reload()
    {
        if (!isReloading)
        {
            if (gunMachine.currentAmmo < gunMachine.maxAmmo)
            {
                base.StartReload();
                anim.SetTrigger(animatorName.RELOAD_MACHINE_GUN);
                gunMachine.Reload();
            }
        }
    }
}
