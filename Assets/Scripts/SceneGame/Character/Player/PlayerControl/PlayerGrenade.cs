using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGrenade : MonoBehaviour
{  
    private Animator anim;
    private bool isAttack;
    private const string AIM_NAME = "Throw";
    [SerializeField] GrenadeController grenade;
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    private void Update()
    {
        if ((Input.GetMouseButtonDown(0)) && (grenade.currentAmmountGrenade > 0))
        {
            if (!isAttack)
            {
                StartAttack();
                anim.SetTrigger(AIM_NAME);
            }   
        }
    }  


    public void ThrowGrenade()
    {
        grenade.ThrowGrenade();
    }
    public void StartAttack()
    {
        isAttack = true;
    }


    public void FinishAttack()
    {
        isAttack = false;
    }
}
