using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKatana : Character
{
    private KatanaColliderDetection katanaColliderDetection;
    [SerializeField] private GameObject ColliderKatata;

    SwapWeapon swapWeapon;
    private void Awake()
    {
        swapWeapon = GetComponent<SwapWeapon>();

        anim = GetComponent<Animator>();

        katanaColliderDetection = ColliderKatata.GetComponent<KatanaColliderDetection>();

    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(!swapWeapon.isDrawing)
            {

            }
            CutSwords1();
        }
      

        if (Input.GetMouseButtonDown(1))
        {
            if (!swapWeapon.isDrawing)
            {
                CutSwords2();
            }   
        }
    }
    private void CutSwords1()
    {
        if (!isAttack)
        {
            katanaColliderDetection.damageKatana = 5f;
            StartAttack();
            anim.SetTrigger("isAttacking1");
        }
    }
    private void CutSwords2()
    {
        if (!isAttack)
        {
            katanaColliderDetection.damageKatana = 15f;
            StartAttack();
            anim.SetTrigger("isAttacking2");
        }
    }

    public void EnableColliderKatana()
    {
        ColliderKatata.SetActive(true);
    }
    public void DisableColliderKatana()
    {
        ColliderKatata.SetActive(false);
    }
}


    
