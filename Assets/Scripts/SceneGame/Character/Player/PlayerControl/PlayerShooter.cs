using Invector.vCharacterController;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;




[System.Serializable]
public class AnimatorName
{
    public string RELOAD_PISTOL = "isReloadAmmoPistol";
    public string RELOAD_MACHINE_GUN = "isReloadAmmoMachineGun";
    public string AIM_PISTOL = "isAim";
    public string AIM_MACHINE_GUN = "isAimMachineGun";
    public string FALL_TO_ROLL = "FallToRoll";
    public string FALLING = "Falling";
    public string DEAD = "Dead";
    public string FELL_DEAD = "FellDead";
}
[SerializeField]


[System.Serializable]
public class NameSounds
{
    //Sound Weapon Shoot
    public string SOUND_DROP_SHELL = "BulletShellDrop";
    public string SOUND_SHOOT ;
}
[SerializeField]
public abstract class PlayerShooter : MonoBehaviour
{
    [SerializeField] protected GameObject crossHair;
    [SerializeField] protected GameObject wayPoint;
    [SerializeField] protected WeaponAim weaponAim;

    [SerializeField] protected float aimSmoothSpeed = 20f;
    [SerializeField] protected LayerMask aimLayerMask;
    [SerializeField] protected GameObject aimPosition;
    [SerializeField] protected Rig aimRig;

    protected vThirdPersonController playerMovement;
    protected Animator anim;
    protected bool isReloading;
   
    public AnimatorName animatorName;
    public NameSounds nameSound;

    protected const string TAG_NAME_ZOMBIE = "Zombie";
    protected const string TAG_NAME_GORES_OBJ = "Gores";
    protected const string TAG_NAME_GRASS_OBJ = "Grass";
    protected const string TAG_NAME_TERRAIN_OBJ = "Terrain";

    private void Awake()
    {
        animatorName = GetComponent<AnimatorName>();
        nameSound = GetComponent<NameSounds>();
    }
    protected virtual void Aim()
    {
        weaponAim.CameraAim();
        playerMovement.AimSpeed();
        aimRig.weight = 1f;
        aimPosition.SetActive(true);
        wayPoint.gameObject.SetActive(false);
        crossHair.SetActive(true);
    }


    protected virtual void NotAim()
    {
        weaponAim.CameraDefault();
        playerMovement.NotAimSpeed();
        aimRig.weight = 0f;
        aimPosition.SetActive(false);
        wayPoint.gameObject.SetActive(true);
        crossHair.SetActive(false);
    }

    protected virtual void StartReload()
    {
        isReloading = true;
        StartCoroutine(Reloading());
    }
    IEnumerator Reloading()
    {
        yield return new WaitForSeconds(1f);
        isReloading = false;   
    }

    protected abstract void ShootAiming();


    protected abstract void Reload();

}
