using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAim : MonoBehaviour
{
    public GameObject mainCamera;


    public float DistanceDefaultAim = 2.5f;
    public float DistanceAim = 1f;
    public float speedZoom = 5f;

    private void Update()
    {
        
    }
    public void AimCamera()
    {
        mainCamera.GetComponent<vThirdPersonCamera>().defaultDistance = Mathf.Lerp(mainCamera.GetComponent<vThirdPersonCamera>().defaultDistance,
        DistanceAim,speedZoom * Time.deltaTime);  
    }

    public void DefaultCamera()
    {
        mainCamera.GetComponent<vThirdPersonCamera>().defaultDistance = Mathf.Lerp(mainCamera.GetComponent<vThirdPersonCamera>().defaultDistance,
        DistanceDefaultAim, speedZoom * Time.deltaTime);
    }
}
