using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyDivingCotroller : MonoBehaviour
{
    public static bool isSkyDiving = true;
    public static bool isCameraSkyDiving = false;

    public GameObject parachute;

    public Rigidbody rb;

    private CameraSkydiving cameraSkydiving;
    private AudioSource soundEffect;
    private bool canPressF = true;
    [SerializeField] private GameObject openCanvas;
    [SerializeField] private GameObject closeCanvas;
    [SerializeField] private GameObject inputMapGuide;

    

    private void Awake()
    {
        parachute.SetActive(false);
        openCanvas.SetActive(true);
        closeCanvas.SetActive(false);
        rb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>();
        soundEffect = GetComponent<AudioSource>();
        cameraSkydiving = GetComponent<CameraSkydiving>();

    }
    private void Update()
    {
        
        if ((Input.GetKeyDown(KeyCode.F)) && (canPressF))
        {
            SoundController.instance.PlayThisSound("CloseParachute", 1f);
            if (isSkyDiving)
            {
                SkyDiving();
            }
            else
            {
                NotSkyDiving();
            }
        }



        if (isCameraSkyDiving)
        {
            cameraSkydiving.CameraParachuting();
        }
        else
        {
            cameraSkydiving.CameraParachuted();
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.transform.CompareTag("Terrain"))
        {
            parachute.GetComponent<Rigidbody>().isKinematic = false;
            parachute.transform.SetParent(null);
            openCanvas.SetActive(false);
            closeCanvas.SetActive(false);
            rb.drag = 0f;
            inputMapGuide.SetActive(true);
            StartCoroutine(DisableScript());
            canPressF = false;
        }
    }

    void SkyDiving()
    {
        parachute.SetActive(true);
        rb.drag = 2f;
        isSkyDiving = false;
        soundEffect.Stop();
        isCameraSkyDiving = true;
        OpenCanvas();
    }
    void NotSkyDiving()
    {
        parachute.SetActive(false);
        rb.drag = 0f;
        isSkyDiving = true;
        isCameraSkyDiving=false;
        CloseCanvas();
    }



    void OpenCanvas()
    {
        openCanvas.SetActive(false);
        closeCanvas.SetActive(true);
    }

    void CloseCanvas()
    {
        openCanvas.SetActive(true);
        closeCanvas.SetActive(false);
    }

    
    IEnumerator DisableScript()
    {
        yield return new WaitForSeconds(2f);
        this.gameObject.SetActive(false);
    }

}
