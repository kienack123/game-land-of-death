using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSkydiving : MonoBehaviour
{
    [SerializeField] private GameObject mainCameraObject;
    private vThirdPersonCamera cameraSkyDiving;

    [SerializeField] private float smoothZoom = 8f;
    [SerializeField] private float defaultCamera = 2.5f;
    [SerializeField] private float zomOutCamera = 20f;


    private void Awake()
    {
        cameraSkyDiving = mainCameraObject.GetComponent<vThirdPersonCamera>();
    }
    public void CameraParachuting()
    {

        cameraSkyDiving.defaultDistance = Mathf.Lerp(cameraSkyDiving.defaultDistance, zomOutCamera, smoothZoom * Time.deltaTime);
    }
    public void CameraParachuted()
    {
        cameraSkyDiving.defaultDistance = Mathf.Lerp(cameraSkyDiving.defaultDistance, defaultCamera, smoothZoom * Time.deltaTime);
    }

    public void CameraResetDistance()
    {
    
        cameraSkyDiving.defaultDistance = defaultCamera;
    
    }
}

