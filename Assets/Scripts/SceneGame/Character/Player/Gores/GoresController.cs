using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoresController : MonoBehaviour
{
    private void OnEnable()
    {
        StartCoroutine(ReturnPool());
    }

    IEnumerator ReturnPool()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }
}
