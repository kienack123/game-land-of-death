
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WeaponWheelButtonController : MonoBehaviour
{
    public int id;

    private Animator anim;

    private bool seteted = false;

    public string itemName;

    public string itemDamage;

    public TextMeshProUGUI itemText;
    public TextMeshProUGUI itemMagazine;

    public Image seletedItem;
     
    public Sprite icon;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();        
    }

    // Update is called once per frame
    void Update()
    {
        if (seteted)
        {
            seletedItem.sprite = icon;
            itemText.text = itemName;
        }

    }
    public void Seleted()
    {
        seteted = true;
    }

    public void Deselected()
    {
        seteted = false;
    }

    public void HoverEnter()
    {
        anim.SetBool("Hover", true);
        itemText.text = itemDamage + "\n"+ itemName +"\n"+ itemMagazine.text;
    }
    public void HoverExit()
    {
        anim.SetBool("Hover", false);
        itemText.text = "";
    }









}
