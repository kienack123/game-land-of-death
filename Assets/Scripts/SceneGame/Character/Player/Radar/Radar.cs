using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radar : MonoBehaviour
{
    [SerializeField] private GameObject[] trackedObject;
    List<GameObject> radarObjects;
    List<GameObject> borderObject;

    [SerializeField] private GameObject radarPrefabs;
    [SerializeField] float swtichDistance = 10;


    [SerializeField] private Transform helpTransform;
 
    void Start()
    {
        CreateRadarObject();

    }

    // Update is called once per frame
    void Update()
    {
        for(int i = 0; i < radarObjects.Count; i++)
        {
            if ((Vector3.Distance(radarObjects[i].transform.position, transform.position)) > swtichDistance)
            {
                helpTransform.LookAt(radarObjects[i].transform);
                borderObject[i].transform.position = transform.position + swtichDistance * helpTransform.forward;
                borderObject[i].layer = LayerMask.NameToLayer("Radar");
                radarObjects[i].layer = LayerMask.NameToLayer("Invisible");
            }
            else
            {
                borderObject[i].layer = LayerMask.NameToLayer("Invisible");
                radarObjects[i].layer = LayerMask.NameToLayer("Radar");
            }
        }
        
        
    }
    protected void CreateRadarObject()
    {
        radarObjects = new List<GameObject>();
        borderObject = new List<GameObject>();
        foreach (GameObject o in trackedObject)
        {
            GameObject k = Instantiate(radarPrefabs,o.transform.position,Quaternion.identity) as GameObject;
            radarObjects.Add(k);
            GameObject j = Instantiate(radarPrefabs, o.transform.position, Quaternion.identity) as GameObject;
            borderObject.Add(j);
        }
    }
}
