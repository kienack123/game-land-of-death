using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KickDetectionCollider : MonoBehaviour
{
    public float damageKick = 2f;
    public float damageForceKick = 1000f;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Zombie") || other.gameObject.CompareTag("HeadShot"))
        {
            HealthZombie healthZombie = other.GetComponent<HealthZombie>();
            healthZombie.TakeDamage(damageKick);
            Rigidbody rigidbodyEnemy = other.GetComponent<Rigidbody>();
            rigidbodyEnemy.AddForce(-transform.forward * 1000f ,ForceMode.Acceleration);
            
        }
    }
}
