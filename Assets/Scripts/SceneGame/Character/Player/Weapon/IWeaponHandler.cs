using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeaponHandler 
{
    void Shoot();

    void Reload();
}
