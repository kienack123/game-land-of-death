using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using MyPooler;
public class Grenade : SubWeapon
{
    public float damageGrenade = 10f;
    // Start is called before the first frame update
    private bool allowExplosde = true;

    private float countDown;

    [SerializeField] private float delay = 3f ;

    [SerializeField] private float radius = 150f;

    [SerializeField] private float force = 700f;

    private string Explosion_Prefab = "Exposion";

    void Start()
    {
        allowExplosde = true;
        countDown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        countDown -= Time.deltaTime;

        if(countDown <= 0f && allowExplosde)
        {
            Explode();
            allowExplosde = false;
        }
    }

    void Explode()
    {
        ObjectPooler.Instance.GetFromPool(Explosion_Prefab, transform.position, Quaternion.identity);

        SoundController.instance.PlayThisSound("soundExplosion",1f);

        Collider[] collider = Physics.OverlapSphere(transform.position, radius);


        foreach (Collider nearbyObject in collider)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            HealthZombie healthEnemy = nearbyObject.GetComponent<HealthZombie>();
            PlayerHealth healthPlayer = nearbyObject.GetComponent<PlayerHealth>();
            if (healthEnemy != null)
            {
                healthEnemy.TakeDamage(damageGrenade);
            }
            if (healthPlayer != null)
            {
                healthPlayer.TakeDamage(damageGrenade);
            }
            if (rb != null)
            {
                rb.AddExplosionForce(force, transform.position, radius);
            }
        }
    }
}
