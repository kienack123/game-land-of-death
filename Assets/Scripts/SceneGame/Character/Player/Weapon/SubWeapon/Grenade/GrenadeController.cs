using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyPooler;
public class GrenadeController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float throwForce = 15f;

    [SerializeField] private Transform rightHand;

    [SerializeField] private Transform mainCamera;
    [SerializeField] private Transform cameraPlayer;

    [SerializeField] AmmoUI uiAmmo;
    [HideInInspector] public int currentAmmountGrenade;
    [HideInInspector] public int maxAmmountGrenade = 5;

    private string grenadeFrefab = "Grenade";
    private void Awake()
    {
        currentAmmountGrenade = maxAmmountGrenade;
    }
    // Start is called before the first frame update
    public void ThrowGrenade()
    {
        GameObject grenade = ObjectPooler.Instance.GetFromPool(grenadeFrefab, transform.position,transform.rotation);

        Rigidbody rb = grenade.GetComponent<Rigidbody>();

        rb.AddForce(cameraPlayer.forward * throwForce,ForceMode.VelocityChange);

        currentAmmountGrenade--;

        uiAmmo.UpdateAmmoGrenade(currentAmmountGrenade);
    }


}
