using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyPooler;
[System.Serializable]
public class KatanaColliderDetection : SubWeapon
{
    public float damageKatana = 1f; 

    protected void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Zombie"))
        {
            HealthZombie healthZombie = other.GetComponent<HealthZombie>();
            healthZombie.TakeDamage(damageKatana);
            ObjectPooler.Instance.GetFromPool("Blood_Explosion", transform.position, transform.rotation);
        }
    }
}
