using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public abstract class MainWeapon : Weapon
{
    [Header("ParameterWeapon")]
    [HideInInspector]
    public float shotRate;
    public float nextTimeShot;
    public float damageBullet;

    public int currentAmmo;
    public int maxAmmo;
    public int magazineSize;

    
    [SerializeField] public AmmoUI ammoUI;
    public WeaponAim weaponAim;
    public EffectWeapon effectWeapon;

    public const string ZOMBIE_NAME = "Zombie";


    public Image crossHairImg;
    private void Awake()
    {
        ammoUI = GetComponent<AmmoUI>();
        crossHairImg = GetComponent<Image>();
    }

    public virtual void Shoot(string _soundShoot , string _soundDropShell)
    {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;

        if ((Time.time > nextTimeShot) && (currentAmmo > 0))
        {
            nextTimeShot = Time.time + shotRate;
            SoundController.instance.PlayThisSound(_soundShoot, 0.5f);
            SoundController.instance.PlayThisSound(_soundDropShell, 0.5f);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.CompareTag(ZOMBIE_NAME))
                {
                    crossHairImg.color = new Color32(255, 0, 0, 255);
                    HealthZombie healthEnemy = hit.transform.GetComponent<HealthZombie>();
                    if (healthEnemy)
                    {
                        healthEnemy.TakeDamage(damageBullet);
                    }
                }
                else if (hit.transform.CompareTag("HeadShot"))
                {
                    crossHairImg.color = new Color32(255, 0, 0, 255);
                    HealthZombie HeadShot = hit.transform.GetComponentInParent<HealthZombie>();
                    if (HeadShot)
                    {
                        HeadShot.HeadShotDead();
                    }
                }
                else if (hit.transform.tag == "ZombieBite")
                {
                    crossHairImg.color = new Color32(255, 0, 0, 255);
                    HealthZombieBite healthZombieBite = hit.transform.GetComponent<HealthZombieBite>();
                    healthZombieBite.TakeDamage(damageBullet);

                }
                else
                {
                    crossHairImg.color = new Color32(255, 255, 255, 255);
                }



                
            }  
            effectWeapon.ShellDrop();
            effectWeapon.FlashMuzzle();
            currentAmmo--;
            ammoUI.UpdateAmmo(currentAmmo, magazineSize);
        }
        
    }

    public virtual void DryOutAmmo()
    {
        if ((Time.time > nextTimeShot))
        {
            nextTimeShot = Time.time + shotRate;
            SoundController.instance.PlayThisSound("gunDryOut", 1f);

        }
    }


    public virtual void Reload()
    {
        StartCoroutine(UpdateUIAmmoReaload());
    }    
    IEnumerator UpdateUIAmmoReaload()
    {
        yield return new WaitForSeconds(1.9f);
        if (magazineSize >= maxAmmo)
        {
            int ammoToReload = maxAmmo - currentAmmo;
            magazineSize -= ammoToReload;
            currentAmmo += ammoToReload;
            ammoUI.UpdateAmmo(currentAmmo, magazineSize);
        }
        else if (magazineSize > 0)
        {
            if (magazineSize + currentAmmo > maxAmmo)
            {
                int leftOverLoad = magazineSize + currentAmmo - maxAmmo;
                magazineSize = leftOverLoad;
                currentAmmo = maxAmmo;
                ammoUI.UpdateAmmo(currentAmmo, magazineSize);
            }
            else
            {
                currentAmmo += magazineSize;
                magazineSize = 0;
                ammoUI.UpdateAmmo(currentAmmo, magazineSize);
            }

        }
    }
}
