using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Reticle : MonoBehaviour
{
    [SerializeField] private RectTransform recticle;

    [Range(20f, 40f)]
    public float size;


    public float restingSize ;
    public float maxSize;
    public float speed;

    public float currrentSize;


    void Start()
    {
        recticle = GetComponent<RectTransform>();

        currrentSize = restingSize;
    }


    void Update()
    {

        if (isMoving || isLooking)
        {
            currrentSize = Mathf.Lerp(currrentSize , maxSize ,Time.deltaTime * speed);  
        }
        else
        {
            currrentSize = Mathf.Lerp(currrentSize, restingSize, Time.deltaTime * speed);
        }

        recticle.sizeDelta = new Vector2(currrentSize, currrentSize);
    }



    bool isMoving
    {
        get
        {
            if(Input.GetAxisRaw("Horizontal") > 0 || Input.GetAxisRaw("Vertical") > 0 )
            {
                return true;
            }
            else
            {
                return false;   
            }
        }
    }

    bool isLooking
    {
        get
        {
            if (Input.GetAxisRaw("Mouse X") != 0 || Input.GetAxisRaw("Mouse Y") != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }






}
