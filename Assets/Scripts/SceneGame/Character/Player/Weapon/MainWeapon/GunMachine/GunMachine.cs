using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunMachine : MainWeapon 
{
    private void Awake()
    {
        shotRate = 0.08f;
        nextTimeShot = 0;
        damageBullet = 1f;
        maxAmmo = 50;
        magazineSize = 350;
        currentAmmo = maxAmmo;
        effectWeapon = gameObject.GetComponent<EffectWeapon>();
    }

    public override void Shoot(string _soundShoot, string _soundDropShell)
    {
        base.Shoot(_soundShoot, _soundDropShell);
    }

    public override void DryOutAmmo()
    {
        base.DryOutAmmo();
    }

    public override void Reload()
    {
        base.Reload();
    }
}
