using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float speedBullet = 15f ;
    private Rigidbody rb;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        Fly();
    }

    void Fly()
    {
        rb.velocity = Vector3.forward * speedBullet * Time.deltaTime;
    }
}
