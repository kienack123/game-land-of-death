using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : MainWeapon
{
    private void Awake()
    {
        shotRate = 0.15f;
        nextTimeShot = 0;
        damageBullet = 1f;
        maxAmmo = 15;
        magazineSize = 150;
        currentAmmo = maxAmmo;
        effectWeapon = GetComponent<EffectWeapon>();
    }
    public override void Shoot(string _soundShoot, string _soundDropShell)
    {
        base.Shoot(_soundShoot, _soundDropShell);
    }


    public override void DryOutAmmo()
    {
        base.DryOutAmmo();
    }
    public override void Reload()
    {
        base.Reload();
    }

}
