using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectWeapon : MonoBehaviour
{
    [SerializeField] private ParticleSystem shellDropEffect;
    [SerializeField] private ParticleSystem flashMuzzleEffect;

    public void ShellDrop()
    { 
        shellDropEffect.Emit(count: 1);
    }
    public void FlashMuzzle()
    {
        flashMuzzleEffect.Play();
    }
}
