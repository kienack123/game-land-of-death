using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class AmmoUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI ammoText;
    [SerializeField] private TextMeshProUGUI ammoGrenadeText;
    public void UpdateAmmo(int currentAmmo, int magazineSize)
    {
        ammoText.text = currentAmmo + "/"+ magazineSize;
    }

    public void UpdateAmmoGrenade(int currentAmmoGrenade)
    {
        ammoGrenadeText.text = "" + currentAmmoGrenade;
    }
}
