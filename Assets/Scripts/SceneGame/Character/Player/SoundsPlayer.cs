using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsPlayer : MonoBehaviour
{
    protected AudioSource swordSound;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void Awake()
    {
        swordSound = GetComponent<AudioSource>();
    }  
    protected void PlaySwordSound()
    {
        SoundController.instance.PlayThisSound("swordSound1", 1f);
    }

    protected void JumpSound()
    {
        SoundController.instance.PlayThisSound("jump", 0.5f);
    }

    protected void PlayReloadAmmoSound()
    {
        SoundController.instance.PlayThisSound("reloadBullet", 1f);
    }

    protected void ThrowGrenadeSound()
    {
        SoundController.instance.PlayThisSound("ThrowGrenade", 1f);
    }

    protected void DrawnKatanaSound()
    {
        SoundController.instance.PlayThisSound("Character/DrawnKatana", 1f);
    }

    protected void FootStep()
    {
        SoundController.instance.PlayThisSound("Character/footsteps", 0.2f);
    }
}
