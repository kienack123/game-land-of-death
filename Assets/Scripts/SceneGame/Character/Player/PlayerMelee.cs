using UnityEngine;
public class PlayerMelee : Character
{
    [SerializeField] private GameObject colliderKick;
    [SerializeField] private KickDetectionCollider kickDetectionCollider;
    
    private void Awake()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            Kick1();
        }


        if (Input.GetKeyDown(KeyCode.X))
        {
            Kick2();
        }
    }

    void Kick1()
    {
        if (!isAttack)
        {
            kickDetectionCollider.damageKick = 1f;
            StartAttack();
            anim.SetTrigger("isKick1");
        }
    }
    void Kick2()
    {
        if (!isAttack)
        {
            kickDetectionCollider.damageKick = 2f;
            StartAttack();
            anim.SetTrigger("isKick2");
        }
    }

    public void EnableDetectionKickCollider()
    {
        colliderKick.SetActive(true);
    }


    public void DisableDetectionKickCollider()
    {
        colliderKick.SetActive(false);
    }


   
}
