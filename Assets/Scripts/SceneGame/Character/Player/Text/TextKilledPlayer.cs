using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Threading;

public class TextKilledPlayer : MonoBehaviour
{
    public TextMeshProUGUI textKilled;
    public int count;    
    private void Start()
    {
        count = 0 ;
    }
    public void UpdateKill()
    {
        count += 1;
        textKilled.text = "Kill :" + count; 
    }
}
