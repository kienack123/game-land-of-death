using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CheckFallDamge : MonoBehaviour
{
    [SerializeField] private  float fallTime = 0;
    [SerializeField] private bool canDie = false ;
    [SerializeField] private bool canTakeDamageFell = false ;
    [SerializeField] private bool isChecked = false;
    [SerializeField] private bool canPressF = true;
    [SerializeField] private GameObject player;
    private float fallDamge;

    [SerializeField] private GameObject suppliesTimer;
    [SerializeField] private GameObject UIplayer;


    private Animator anim;
    private bool isChangeLayerAnim =false;


    private Rigidbody rigibody;
    private PlayerHealth playerHealth;
    private void Awake()
    {
        anim = player.GetComponent<Animator>();
        rigibody = player.GetComponent<Rigidbody>();
        playerHealth = GetComponent<PlayerHealth>();
        anim.SetTrigger("Falling");
        UIplayer.SetActive(false);
    }
    private void FixedUpdate()
    {  
        CheckVelocityPlayer();

        // 3 state check fall
        if (fallTime > 4)
        {
            canDie = true;
            canTakeDamageFell = false;
        }
        else if (fallTime >= 0.01f && fallTime <= 4)
        {
            canTakeDamageFell = true;
            canDie = false;
        }
        else if (fallTime <= 1)
        {
            canTakeDamageFell = false;
            canDie = false;
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && canPressF )
        {
            fallTime = 0;
            isChecked = !isChecked;
            isChangeLayerAnim = true;
        }

        if (isChangeLayerAnim)
        {
            anim.SetLayerWeight(5, Mathf.Lerp(anim.GetLayerWeight(5), 0f, Time.deltaTime * 2f));
        }
    }
    void CheckVelocityPlayer()
    {
        // If the velocity is negative
        if (!isChecked)
        {
            if (rigibody.velocity.y < 0)
            {
                // Count the time as "fall time"
                fallTime += Time.deltaTime;
            }
        }
    }
    void TakeDamageFell(float fallDamge)
    {      
        playerHealth.TakeDamage(fallDamge);
        fallTime = 0;
        anim.SetTrigger("FallToRoll");
    }
    void FellDead()
    {
        anim.SetTrigger("FellDead");
        //DisableAllScripts();
        SceneManager.LoadScene(3);
    }
    IEnumerator EnableUI(float timeActive)
    {
        yield return new WaitForSeconds(timeActive);
        UIplayer.SetActive(true);
        suppliesTimer.SetActive(true);
      
    }
    


    private void OnCollisionEnter(Collision collision)
    {
        if ((collision.gameObject.CompareTag("Terrain")) && (canDie))
        {
            FellDead();
            isChecked = true;
            canPressF = false;
        }
        if ((collision.gameObject.CompareTag("Terrain")) && (canTakeDamageFell))
        {
            fallDamge = fallTime * 2f;
            TakeDamageFell(fallDamge);
            StartCoroutine(EnableUI(0.4f));
            isChecked = true;
            canPressF = false;
        }
        if ((collision.gameObject.CompareTag("Terrain")) && (!canTakeDamageFell) && (!canDie))
        {
            isChecked = true;
            StartCoroutine(EnableUI(0.4f));
            canPressF = false;
        }
    }
}
