using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private float maxHealth = 100;
    [SerializeField] private float currentHealth;
    [SerializeField] private Slider playerHealthSlider;
    [SerializeField] private GameObject HurtUI;




    private Animator anim;
    private void Awake()
    {
        anim = GetComponent<Animator>();    
        currentHealth = maxHealth;
        playerHealthSlider.maxValue = maxHealth;
        playerHealthSlider.value = maxHealth; 
    }

    // Update is called once per frame
    private void Update()
    {
        if (currentHealth <= 0f)
        {
            Dead();
        }
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        playerHealthSlider.value = currentHealth;
        StartCoroutine(OnHurtUI());
    }

    public void Dead()
    {
        anim.SetTrigger("Dead");
        Debug.Log("You Dead");
        SceneManager.LoadScene(3);
        DisableAllScripts();
    }
    void DisableAllScripts()
    {
        MonoBehaviour[] scripts = gameObject.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = false;
        }
    }
    IEnumerator OnHurtUI()
    {
        yield return null;
        HurtUI.SetActive(true);
        yield return new WaitForSeconds(1f);
        HurtUI.SetActive(false);
    }
}
