using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueTextNPC2 : DialogueTextNPC
{
    public GameObject zombieBite;
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player") && !endTalk && !zombieBite)
        {
            dialogue.SetActive(true);
            dialoguePlayer.SetActive(true);
            StartCoroutine(Talking());
            anim.SetBool("isStandUp", true);
        }
    }

  
    protected override IEnumerator Talking()
    {
        yield return new WaitForSeconds(2f);
        text.text = "Thank you ,sir";

        yield return new WaitForSeconds(2f);
        textPlayer.text = "Take my weapon and fight against them with me,man ";
        TakeWeapon();

        yield return new WaitForSeconds(2f);
        text.text = "i got it ,what do we next";

        yield return new WaitForSeconds(2f);
        textPlayer.text = "Let's go , Helicopter will be there in 30 minutes ";

        yield return new WaitForSeconds(2f);
        textPlayer.text = "Before that save everyone and get out of here ";

        yield return new WaitForSeconds(2f);
        text.text = "ok , i will cover you";

        yield return new WaitForSeconds(2f);
        dialogue.SetActive(false);
        dialoguePlayer.SetActive(false);
        endTalk = true;
        
    }

    public virtual void TakeWeapon()
    {
        weapon.SetActive(true);
        playerWeapon.SetActive(false);
    }
    
}
