using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class NPC2Controller : NPC
{
    DialogueTextNPC2 dialogueText;
    private void Awake()
    {
        dialogueText = GetComponent<DialogueTextNPC2>();
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (savedByPlayer)
        {
            float distance = Vector3.Distance(transform.position, player.transform.position);

            if (dialogueText.endTalk == true)
            {
                if ((distance < distanceStop))
                {
                    anim.SetBool("isRunning", false);
                    agent.isStopped = true;
                }
                else
                {
                    agent.SetDestination(player.transform.position);
                    Follow();
                }
            }
        }

    }
}
