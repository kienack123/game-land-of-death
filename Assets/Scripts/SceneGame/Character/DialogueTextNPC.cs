using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public abstract class DialogueTextNPC : MonoBehaviour
{
    public GameObject dialoguePlayer;
    public TextMeshProUGUI textPlayer;
    protected Animator anim;

    public TextMeshProUGUI text;
    public GameObject dialogue;

    public bool endTalk = false;

    public GameObject weapon;
    public GameObject playerWeapon;

    protected abstract IEnumerator Talking();
}
