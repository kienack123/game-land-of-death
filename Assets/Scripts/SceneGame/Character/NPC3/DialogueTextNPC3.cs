using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueTextNPC3 : DialogueTextNPC2
{
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player") && !endTalk)
        {
            dialogue.SetActive(true);
            dialoguePlayer.SetActive(true);
            StartCoroutine(Talking());
            anim.SetBool("isStandUp", true);
        }
    }

    protected override IEnumerator Talking()
    {
     
        yield return new WaitForSeconds(2f);
        textPlayer.text = "I'm here to save you, don't worry";

        yield return new WaitForSeconds(2f);
        text.text = "Thank you";

        yield return new WaitForSeconds(2f);
        textPlayer.text = "Take my weapon and follow me ,hurry before they come again";
        TakeWeapon();

        yield return new WaitForSeconds(2f);
        text.text = "ok";

        yield return new WaitForSeconds(2f);
        textPlayer.text = "Let's go ";

        yield return new WaitForSeconds(2f);
        dialogue.SetActive(false);
        dialoguePlayer.SetActive(false);
        endTalk = true;

        TakeWeapon();

    }


    public override void TakeWeapon()
    {
        weapon.SetActive(true);
    }
    
}
