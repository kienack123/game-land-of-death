using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPC : MonoBehaviour
{
    [SerializeField] protected Transform player;
    [HideInInspector] public NavMeshAgent agent;
    [HideInInspector] public Animator anim;
    public bool savedByPlayer = true;

    public float distanceStop;

    public float speed;

    public void Follow()
    {
        anim.SetBool("isRunning", true);
        agent.isStopped = false;
        agent.speed = speed;
    }
    public void InjuredIdle()
    {
        anim.SetBool("isIdle", true);
    }

    public void Idle()
    {
        anim.SetBool("isIdle", true);
    }

    public void Talk()
    {
        anim.SetBool("isTalking", true);
    }


}
