using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using static UnityEngine.Rendering.DebugUI;

public class DialogueText : DialogueTextNPC
{
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player") &&!endTalk)
        {
            dialogue.SetActive(true);
            dialoguePlayer.SetActive(true);
            StartCoroutine(Talking());
            anim.SetBool("isStandUp", true);
        }
    }
    protected override IEnumerator Talking()
    {
        yield return new WaitForSeconds(2f);
        textPlayer.text = "are you ok ?";

        yield return new WaitForSeconds(2f);
        text.text = "im' scary ,please get me out of here";

        yield return new WaitForSeconds(2f);
        textPlayer.text = "What's going on ?";

        yield return new WaitForSeconds(1f);
        text.text = "I don't know ";

        yield return new WaitForSeconds(2f);
        text.text = "Zombie suddenly appearered and attacked us";

        yield return new WaitForSeconds(2f);
        textPlayer.text = "Where are the others";

        yield return new WaitForSeconds(2.5f);
        text.text = "i'm not sure ,maybe they all turnd into zombies ";

        yield return new WaitForSeconds(2.5f);
        textPlayer.text = "follow me , i will protect you";

        yield return new WaitForSeconds(2.5f);
        textPlayer.text = "Now ,we'll go look for someone else alive";

        yield return new WaitForSeconds(2.5f);
        textPlayer.text = "let's go";


        yield return new WaitForSeconds(1f);
        dialogue.SetActive(false);
        dialoguePlayer.SetActive(false);
        endTalk = true;
    }

}
