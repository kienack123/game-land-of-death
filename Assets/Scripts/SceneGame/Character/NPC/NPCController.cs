using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class NPCController : NPC
{
    DialogueText dialogueText;
    private void Awake()
    {
        dialogueText = GetComponent<DialogueText>();
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        float distance = Vector3.Distance(transform.position, player.transform.position);
        if ((distance < 2.5) && (dialogueText.endTalk == false))
        {
            anim.SetBool("isStandUp", true);
            agent.isStopped = true;
        }
        if (dialogueText.endTalk == true)
        {
            if((distance < distanceStop))
            {
                anim.SetBool("isRunning", false);
                agent.isStopped = true;
            }
            else
            {
                agent.SetDestination(player.transform.position);
                Follow();
            }
        }

    }
}
