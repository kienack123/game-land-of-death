using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public abstract class Character : MonoBehaviour
{
    public bool isAttack;
    public Animator anim;
    public Rigidbody rb;

    public void BlockMovement()
    {
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
    }
    public void UnBlockMovement()
    {
        rb.constraints = RigidbodyConstraints.None | RigidbodyConstraints.FreezeRotation;
    }

    public void StartAttack()
    {
        isAttack = true;
    }

    public void FinishAttack()
    {
        isAttack = false;
    }

}
