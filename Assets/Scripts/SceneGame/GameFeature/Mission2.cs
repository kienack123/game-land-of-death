using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mission2 : MonoBehaviour
{
    public DialogueTextNPC2 dialogueTextNPC2;   
    public bool finishMisson2 = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player") && dialogueTextNPC2.endTalk == true)
        {
            finishMisson2 = true;
        }    
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.transform.CompareTag("Player")  && dialogueTextNPC2.endTalk == true)
        {
            finishMisson2 = true;
        }
    }
}
