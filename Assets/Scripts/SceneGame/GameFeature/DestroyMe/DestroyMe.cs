using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMe : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private float timeAlive = 10;
    private void OnEnable()
    {
        Destroy(gameObject, timeAlive);
    }
}
