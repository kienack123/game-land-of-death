using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneManagerController: MonoBehaviour
{
    public static SceneManagerController Instance { get; private set; }

    private bool gameIsPause = false;

    HideCursor hideCursor;

    [SerializeField] private GameObject mainMenu;

    [SerializeField] vThirdPersonCamera cam;

    private void Awake()
    {
        Time.timeScale = 1f;
        hideCursor = GetComponent<HideCursor>();
        if (Instance)
        {
            Destroy(gameObject);
        }else
        {
            Instance = this;
        } 
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPause)
            {
                Resume();

            }
            else
            {
                Pause();
            }
        }
    }

    public IEnumerator GameOver()
    {
        yield return new WaitForSeconds(2.5f);

        SceneManager.LoadScene(3);
    }


    public void ResetGame()
    {
        SceneManager.LoadScene(3);
    }

    void Pause()
    {
        cam.lockCamera = true;
        hideCursor.Show();
        mainMenu.SetActive(true);
        gameIsPause = true;
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        cam.lockCamera = false;
        hideCursor.Hide();
        mainMenu.SetActive(false);
        gameIsPause = false;
        Time.timeScale = 1f;
    }
}

