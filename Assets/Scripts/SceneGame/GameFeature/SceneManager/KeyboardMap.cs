using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMap : MonoBehaviour
{
    public GameObject playerCanvas;
    public GameObject keyboardMap;
    public GameObject inputMapGuidde;

    private void Awake()
    {
        keyboardMap.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            if(inputMapGuidde!= null)
            {
                inputMapGuidde.SetActive(false);         
            }
            playerCanvas.SetActive(!playerCanvas.activeSelf);
            keyboardMap.SetActive(!keyboardMap.activeSelf);
            SoundController.instance.PlayThisSound("ChangeWeapon", 1f);
        }
        
    }
}
