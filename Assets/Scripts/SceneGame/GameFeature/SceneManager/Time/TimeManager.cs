using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class TimeManager : MonoBehaviour
{
    //[SerializeField] private float slowMotionTimeScale = 0.05f;
    public void StartSlowTime()
    {
        Time.timeScale = Mathf.Lerp(1, 0.1f, 5);
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
    }
    public void ReturnRealTime()
    {
        Time.timeScale = Mathf.Lerp(0.1f, 1, 5);
    }
}
