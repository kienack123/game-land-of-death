using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Compass : MonoBehaviour
{
    [SerializeField] private RawImage CompassImage;
    [SerializeField] private Transform Player;
    [SerializeField] private Text CompassDirectionText;

   
    void Start()
    {
		CompassDirectionText.gameObject.GetComponent<Text>();
    }

    void Update()
    {

		CompassImage.uvRect = new Rect(Player.localEulerAngles.y / 360, 0, 1, 1);

		Vector3 forward = Player.transform.forward;

	
		forward.y = 0;

	
		float headingAngle = Quaternion.LookRotation(forward).eulerAngles.y;
		headingAngle = 5 * (Mathf.RoundToInt(headingAngle / 5.0f));

	
		int displayangle ;
		displayangle = Mathf.RoundToInt(headingAngle);

		switch (displayangle)
		{
			case 0:
				
				CompassDirectionText.text = "N";
                CompassDirectionText.color = Color.yellow;
                break;
			case 360:
			
				CompassDirectionText.text = "N";
                CompassDirectionText.color = Color.yellow;
                break;
			case 45:
				CompassDirectionText.text = "NE" ;
				CompassDirectionText.color = Color.white;
                break;
			case 90:
		
				CompassDirectionText.text = "E";
                CompassDirectionText.color = Color.white;
                break;
			case 130:
		
				CompassDirectionText.text = "SE";
                CompassDirectionText.color = Color.white;
                break;
			case 180:
				
				CompassDirectionText.text = "S";
                CompassDirectionText.color = Color.white;
                break;
			case 225:
			
				CompassDirectionText.text = "SW";
                CompassDirectionText.color = Color.white;
                break;
			case 270:
				
				CompassDirectionText.text = "W";
                CompassDirectionText.color = Color.white;
                break;
			default:
				CompassDirectionText.text = headingAngle.ToString();
                CompassDirectionText.color = Color.white;
                break;
		}


	}
}
