using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public static SoundController instance { get; private set; }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        
    }

    public void PlayThisSound(string clipName, float volumeMutiplier)
    {
        AudioSource audioSource = this.gameObject.AddComponent<AudioSource>();

        audioSource.volume *= volumeMutiplier;

        audioSource.PlayOneShot((AudioClip)Resources.Load("Sounds/" + clipName, typeof(AudioClip)));


    }
}
