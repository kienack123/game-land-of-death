
using UnityEngine;

public class MouseLookAtAround : MonoBehaviour
{
    public float rotationSpeed = 2f;
    public Transform target, player;
    float mouseX;
    float mouseY;
    float h;
    float v;

    // Start is called before the first frame update
    void Start()
    {
      
    }

    void LateUpdate()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");
        mouseX += Input.GetAxis("Mouse X") * rotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * rotationSpeed;
        mouseY = Mathf.Clamp(mouseY, -35, 60);
        CamControl();
    }

    void CamControl()
    {
        target.position = player.position;

        transform.LookAt(target);
        if (h != 0 || v != 0)
        {
            target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
            player.rotation = Quaternion.Euler(0, mouseX, 0);
        }
        else
        {
            target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
            while (h != 0 || v != 0)
            {
                target.rotation = Quaternion.Euler(target.rotation.x, 0, target.rotation.z);
                player.rotation = Quaternion.Euler(player.rotation.x, player.rotation.y - target.rotation.y + 180, player.rotation.z);
            }

        }
    }
}
