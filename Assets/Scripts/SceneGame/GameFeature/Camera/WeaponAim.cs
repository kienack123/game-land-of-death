using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAim : MonoBehaviour
{
    [Header("Zoom Aim Camera")]
    public float zoomSpeed = 8f;
    public float zoomAim = 0;
    public float zoomDefault;
    vThirdPersonCamera cameraAim;

    private void Awake()
    {
        cameraAim = GetComponent<vThirdPersonCamera>();
        zoomDefault = cameraAim.defaultDistance;
    }
    public void CameraAim()
    {
        cameraAim.defaultDistance = Mathf.Lerp(cameraAim.defaultDistance, zoomAim, zoomSpeed * Time.deltaTime);

        cameraAim.rightOffset = Mathf.Lerp(cameraAim.rightOffset, 0.75f, zoomSpeed * Time.deltaTime);

        cameraAim.height = Mathf.Lerp(cameraAim.height, 1.6f, zoomSpeed * Time.deltaTime);

    }
    public void CameraDefault()
    {
        cameraAim.defaultDistance = Mathf.Lerp(cameraAim.defaultDistance, zoomDefault, zoomSpeed * Time.deltaTime);

        cameraAim.rightOffset = Mathf.Lerp(cameraAim.rightOffset, 0f, zoomSpeed * Time.deltaTime);

        cameraAim.height = Mathf.Lerp(cameraAim.height, 1.4f, zoomSpeed * Time.deltaTime);

    }



}
