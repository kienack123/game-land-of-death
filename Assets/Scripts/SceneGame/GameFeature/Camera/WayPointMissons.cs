using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.UI;
public class WayPointMissons : MonoBehaviour
{
    [SerializeField] protected Image img;

    public Transform target;

    [SerializeField] protected Text meter;

    [SerializeField] protected Vector3 offset;

    public Transform mission1 ;
    public Transform mission2 ;
    public Transform mission3 ;

    public DialogueText dialogueText;
    public Mission2 missionFinish;
    private void Awake()
    {
        target = mission1;
    }

    void Update()
    {
        if (dialogueText.endTalk == true)
        {
            target = mission2;
        }
        if (missionFinish.finishMisson2 == true)
        {
            target = mission3;
        }
        float minX = img.GetPixelAdjustedRect().width / 2;
        float maxX = Screen.width - minX;
        float minY = img.GetPixelAdjustedRect().height / 2;
        float maxY = Screen.height - minY;

        Vector2 pos = Camera.main.WorldToScreenPoint(target.position + offset);

        if (Vector3.Dot((target.position - transform.position), transform.forward) < 0)
        {
            if (pos.x < Screen.width / 2)
            {
                pos.x = maxX;
            }
            else
            {
                pos.x = minX;
            }

        }

        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        pos.y = Mathf.Clamp(pos.y, minY, maxY);


        img.transform.position = pos;


        meter.text = ((int)Vector3.Distance(target.position, transform.position)).ToString() +" m";
    }
}
