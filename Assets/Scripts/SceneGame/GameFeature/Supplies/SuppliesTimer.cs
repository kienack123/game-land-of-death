using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SuppliesTimer : MonoBehaviour
{
    public float timeLeft;
    public TextMeshProUGUI timeTxt;
    
    public bool timeOn = false;

    // Update is called once per frame

    private void Start()
    {
        timeOn = true;
    }
    void Update()
    {
        if (timeOn)
        {
            if(timeLeft > 0)
            {
                timeLeft -= Time.deltaTime;
                UpdateTimer(timeLeft);
            }
            else
            {
                Debug.Log("Time is up !");
                timeLeft = 0;
                timeOn = false;
            }
        }
    }
    void UpdateTimer(float currentTime) 
    {
        currentTime += 1;
        float minutes = Mathf.FloorToInt(currentTime / 60);
        float seconds = Mathf.FloorToInt(currentTime % 60);

        timeTxt.text = string.Format("{0:00} : {1:00}", minutes, seconds);
    }

}
