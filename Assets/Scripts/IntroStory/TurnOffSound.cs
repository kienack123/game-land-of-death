using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffSound : MonoBehaviour
{
    private GameObject soundSceneLoad;
    private void OnEnable()
    {
        soundSceneLoad = GameObject.FindGameObjectWithTag("MusicLoadScene");
        if (soundSceneLoad != null)
        {
            Destroy(soundSceneLoad);
        }
    }
}
