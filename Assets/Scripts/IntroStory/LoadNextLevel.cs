using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextLevel : MonoBehaviour
{
    public int sceneIndex;
    private void OnEnable()
    {
        SceneManager.LoadSceneAsync(sceneIndex);
    }

}
