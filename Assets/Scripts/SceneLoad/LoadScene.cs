using UnityEngine;
using UnityEngine.SceneManagement;
public class LoadScene : MonoBehaviour
{
    public int nextScene;
    private void OnEnable()
    {
        SceneManager.LoadScene(nextScene);
    }
}
